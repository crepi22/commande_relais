/*
 * Temporisateur programmable à 12 sorties
 *
 * Pour chaque sortie on peut configurer le temps à zero et à un en
 * dixième de seconde.
 *
 * Configuration de la sortie série 115200.
 * Commandes disponibles
 *
 * s <pin> <duree à zero> <durée à un> modifie l'entrée pin pour
 * r <pin> remet à 0 le compteur courant.
 * p : imprime la configuration courante
 */
#include <EEPROM.h>

// Première sortie
const int first = 2;
// Dernière sortie + 1. Modifiable si pas besoin de 12 sorties
const int last = 14;
// Taille du buffer de commandes
const int COMLEN = 255;
// Position currente du buffer
int pos = 0;

// Durée en dizième de secondes des temps à 0 de chaque pin
unsigned int delai_zero[last - first];
// Durée en dizième de secondes des temps à 1 de chaque pin
unsigned int delai_un[last - first];
// Compteur dynamique associé à chaque pin
unsigned int maintenant[last - first];
// Buffer de commande pour la configuration
char command[COMLEN];

void setup() {
  Serial.begin(38400);
  for(int pin=first; pin < last; pin++) {
    // On décale car 0 et 1 pris par serie.
    int indice = pin - first;
    // Configuration du pin en sortie
    pinMode(pin, OUTPUT);
    // Lecture des temps d'attente de l'EEPROM: 4 octets par pin
    EEPROM.get(indice * 4, delai_zero[indice]);
    EEPROM.get(indice * 4 + 2, delai_un[indice]);
    // On initialise le compteur.
    maintenant[indice] = 0;
  }
  pinMode(9, OUTPUT);
  digitalWrite(9, HIGH);
}

unsigned int parseInt() {
  char *c = strtok(NULL, " ");
  if (c == NULL) {
    return 0xFFFF;
  }
  char *n;
  unsigned int result = (unsigned int) strtol(c, &n, 10);
  if (n == c) {
    return 0xFFFF;
  }
  return result;
}

void handleCommand() {
  char *c = strtok(command, " ");
  if(c[0]=='s') {
    int pin = parseInt();
    if(pin < 1 || pin > last - first) {
      Serial.print("Mauvaise sortie: 1-");
      Serial.println(last - first);
      return;
    }
    unsigned int zero = parseInt();
    if (zero > 65000) {
      Serial.println("Mauvaise durée (état bas): 0-65000");
      return;
    }
    unsigned int un = parseInt();
    if (un > 65000) {
      Serial.println("Mauvaise durée (état haut): 0-65000");
      return;
    }
    int indice = pin - 1;
    delai_zero[indice] = zero;
    delai_un[indice] = un;
    EEPROM.put(indice * 4, delai_zero[indice]);
    EEPROM.put(indice * 4 + 2, delai_un[indice]);
    Serial.println("ok");
    return;
  }

  if(c[0]=='p') {
    Serial.println("Configuration:");
    for(int i = 0; i < last - first; i++) {
      Serial.print(i+1);
      Serial.print(" ");
      Serial.print(delai_zero[i]);
      Serial.print(" ");
      Serial.println(delai_un[i]);
    }
    return;
  }
  if(c[0]=='r') {
    int pin = parseInt();
    if(pin < 1 || pin > last - first) {
      Serial.print("Mauvaise sortie: 1-");
      Serial.println(last - first);
      return;
    }
    maintenant[pin - 1] = 0;
  }
  Serial.println("Pas reconnu s/p");
}

void loop() {
  while(Serial.available()) {
    char c = Serial.read();
    if (c == '\r' || c == '\n') {
      command[pos] = 0;
      Serial.print("\r\n");
      handleCommand();
      pos = 0;
    } else if (c == '\003') {
      Serial.print("^C\r\n");
      pos = 0;
    } else {
      Serial.print(c);
      Serial.flush();
      command[pos++] = c;
      // Protection anti chat/bébé..
      if (pos > COMLEN) pos = 0;
    }
  }
  for(int pin=first; pin < last; pin++) {
    int indice = pin - first;
    unsigned int dz = delai_zero[indice], du = delai_un[indice], ma = maintenant[indice];
    /* Mémoire eeprom non initialisée. On passe */
    if (dz == 0xFFFF || du == 0xFFFF) continue;
    if (ma == 0) {
      digitalWrite(pin, LOW);
    } else if (ma == dz) {
      digitalWrite(pin, HIGH);
    }
    maintenant[indice] = (ma + 1) % (dz + du);
  }
  delay(100);
}
