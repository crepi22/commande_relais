# Commande de relais configurable par l'interface série

Le système permet de configurer 12 sorties sur les pins numérotés 2 à 13.
Chaque sortie oscille entre un état bas et un état haut durant des durées
déterminées. Le système est utilisé pour faire fonctionner des trains en
va et vient.

## Configuration

On utilisera les nombres de 1 à 12 pour désigner ces entrées. On utilise
la sortie série de l'arduino normalement en passant par un adapteur USB série.

La sortie série est configurée à 38400 bauds 8 bits sans parité et un bit de
stop.

* `s i d1 d2` configurera l'entrée `i` pour qu'elle alterne entre l'état bas
  pendant `d1` dixièmes de seconde et l'état haut pendant `d2` dixièmes de 
  seconde. `d1` et `d2` doivent être inférieurs à 64000.
* `p` affiche la configuration des entrées
* `r i` resette l'entrée `i` (c'est à dire recommence au début d'une période à
  l'état 0)

Les valeurs configurées sont stockées dans l'EEPROM de l'arduino et sont donc
réutilisées à chaque remise sous tension.

## Mise en oeuvre de l'interface série

Avec un téléphone mobile on peut utiliser un cable USB OTG (On the go) et un
cable standard vers mini USB (ou micro USB/usb-c pour les arduino nano récents).
Il faut une application comme 'Serial USB Terminal'. Il faut configurer la
connexion avec les paramètres donnés ci dessus.

Avec un PC sous Linux. On peut soit utiliser la console de l'IDE Arduino
(accessible en appuyant sur Ctrl-Shift-M) ou avec par exemple cu

    cu -l /dev/ttyUSB0 -s 38400

Les commandes tapées ne sont pas visibles. Seul le résultat est affiché. Pour
quitter le programme utiliser ~.

## Mise en oeuvre matérielle

Le circuit utilisé peut être schématisé de la manière suivante. Il faudrait
un deuxième ULN2803 pour utiliser toutes les entrées (En pratique sur
le réseau cible, il n'y aura que 3 ou 4 sorties utilisées). Noter le
branchement de la borne 12 de l'ULN2803 sur 12V pour utiliser les diodes
internes de protection contre les surtensions venant des bobines des relais.

        Txd  Arduino  Vin - +12V              L1 1         16 H1
        Rxd   Nano    GND - Gnd               ...  ULN2803 ...
        Reset       Reset                     L8 8         11 H8
        Gnd            5V                    GND 9         12 +12V
    L1  - D2
    ...
    L11 -D12            D13 - L12

Chaque relai peut être vu comme transmettant des entrées I1 et I2 vers O1 et O2
respectivement ou vers O2 O1 resp. quand le relai est inversé. L'entrée 1 de
commande est reliée au 12V, la sortie 16 est reliée à la terre via la sortie
Hi de l'ULN2803 et le circuit ne sera donc fermé que si Li est à l'état haut.
Noter que les sorties sont reliées en croix à 8 et 11 d'une part pour O1 et 6
9 d'autre part pour O2.

    12V -  1    Relai i     16 - Hi
            HK19F-DC12V
    I1  -  4                13 - I1
    
    O2  -  6                11 - O1
    
    O1  -  8                 9 - O2
